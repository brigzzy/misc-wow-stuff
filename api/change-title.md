## Changing your title


### What it does

Changes your title from a macro.  There's the /settitle command, but this works better (can't /settitle Shado-master for example)

You can get the titles from this API:

https://us.api.blizzard.com/data/wow/title/index?namespace=static-us&locale=en_US&access_token=US8zmp8KboUruYbL44zh5AUpqTIDg8fnHh 

### The Code 

#### Set a title by ID

    /run SetCurrentTitle(205);

#### Set a Random title

    /run SetCurrentTitle(random(419));

#### Set a random title (in a better way)

    /run local t={}; for titles=1,GetNumTitles()-1,1 do if IsTitleKnown(titles) then table.insert(t,titles) end; end; SetCurrentTitle(t[random(#t)]);